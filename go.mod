module gitlab.com/Alexandrhub/tradestrategy

go 1.19

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/chi/v5 v5.0.10
	github.com/joho/godotenv v1.5.1
	gitlab.com/beearn/datawarehouse v1.0.3
	gitlab.com/beearn/entity v1.0.1
	gitlab.com/ptflp/gopubsub v0.0.0-20230816140942-31116ca4e9e5
	google.golang.org/appengine v1.6.7
)

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230822172742-b8732ec3820d // indirect
	google.golang.org/grpc v1.58.1 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
