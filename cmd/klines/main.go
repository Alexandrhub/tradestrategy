package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/joho/godotenv"
	"gitlab.com/Alexandrhub/tradestrategy/modules/klines/repository"
	"gitlab.com/Alexandrhub/tradestrategy/modules/klines/worker"
	"gitlab.com/ptflp/gopubsub/kafkamq"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	storage := repository.NewDashboardStorage()

	mq, err := kafkamq.NewKafkaMQ("localhost:9092", "myGroup")
	if err != nil {
		log.Fatalf("Failed to create MQ: %s\n", err)
	}

	ch, err := mq.Subscribe(worker.NewKlinesData)
	if err != nil {
		log.Fatalf("Failed to subscribe: %s\n", err)
	}

	syncWorker := worker.NewKlinesSync(
		worker.KlinesSyncConfig{
			Addr:    fmt.Sprintf("%s:%s", os.Getenv("WAREHOUSE_IP"), os.Getenv("WAREHOUSE_PORT")),
			Timeout: 30 * time.Second,
			Storage: storage,
			PubSub:  mq,
		},
	)

	go func() {
		for {
			msg := <-ch
			var v kafka.Error
			ok := errors.As(msg.Err, &v)
			if ok && v.Code() == kafka.ErrUnknownTopicOrPart {
				fmt.Println("waiting incoming data from kafka")
				time.Sleep(1 * time.Minute)
			}
			fmt.Println(string(msg.Data))
			err := mq.Ack(&msg)
			if err != nil {
				log.Printf("failed to ack: %s", err)
			}
		}
	}()

	syncWorker.Sync()
}
