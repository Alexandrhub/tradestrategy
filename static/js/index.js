var charts = [];
var strategyEnabled = document.getElementById('strategy');
var timePeriodSelect = document.getElementById('time_period');


function buildDropdown(data) {
    const mainWrapper = document.querySelector('.main-wrapper');
    const dropdown = mainWrapper.querySelector('.dropdown');
    while (dropdown.firstChild) {
        dropdown.removeChild(dropdown.firstChild);
    }
    // Кнопка для показа списка
    const dropbtn = document.createElement('button');
    dropbtn.className = 'dropbtn';
    dropbtn.innerText = 'BTCUSDT';  // Можно заменить на другое значение по умолчанию
    dropbtn.onclick = myFunction;
    dropdown.appendChild(dropbtn);

    // Контейнер для списка и поля ввода
    const dropdownContent = document.createElement('div');
    dropdownContent.id = 'myDropdown';
    dropdownContent.className = 'dropdown-content';

    // Поле ввода для фильтрации списка
    const input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Search..';
    input.id = 'myInput';
    input.onkeyup = filterFunction;
    dropdownContent.appendChild(input);

    // Добавляем каждый элемент из данных в список
    data.forEach(item => {
        const a = document.createElement('a');
        a.href = '#';
        a.innerText = item;
        a.onclick = function() {
            dropbtn.innerText = item; // Обновляем текст кнопки
            document.getElementById("myDropdown").classList.toggle("show"); // Скрываем список
            CreateDashboard(item); // Создаем дашборд для выбранного элемента
        };
        dropdownContent.appendChild(a);
    });

    dropdown.appendChild(dropdownContent);
}

// Затем вызываем функцию fetch и построения списка:

fetch('/api/v1/symbols')
    .then(response => response.json())
    .then(data => {
        buildDropdown(data);
    })
    .catch(error => {
        console.error('Error fetching data:', error);
    });

var periodExtremesMinutes = {
    '1m': 1,
    '15m': 15,
    '1h': 60,
    '4h': 240,
    '12h': 720,
    '1d': 1440,
    '1w': 10080,
    '1M': 43200
}

function CreateDashboard(symbol, autoReload = false) {
    charts = [];
    let url = `/api/v1/klines?symbol=${symbol}`;
    if (autoReload) {
        url += '&autoReload=true';
    }
    if (strategyEnabled.checked) {
        const selectedTimePeriodValue = timePeriodSelect.options[timePeriodSelect.selectedIndex].value;
        url += `&strategy=true&timePeriod=${selectedTimePeriodValue}`;
    }

    Highcharts.getJSON(url, function (data) {
        const periods = ['1m', '15m', '1h', '4h', '12h', '1d', '1w', '1M'];

        const tab = document.querySelector('.tab');
        while (tab.firstChild) {
            tab.removeChild(tab.firstChild);
        }
        periods.forEach(period => {
            CreateChart(data, period, symbol);
        });
    });
}

setInterval(function() {
    if (document.getElementById('reload').checked) {
        const symbol = document.querySelector('.dropbtn').innerText;
        CreateDashboard(symbol, true);
    }
}, 13000);


// Добавить событие `onClick`
strategyEnabled.addEventListener('click', function() {
    // Если флажок выбран, то отключить стратегию
    if (strategyEnabled.checked) {
        const symbol = document.querySelector('.dropbtn').innerText;
        CreateDashboard(symbol, false);
    } else {
        const symbol = document.querySelector('.dropbtn').innerText;
        CreateDashboard(symbol, true);
    }
});

CreateDashboard('BTCUSDT');

function CreateChart(data, period, symbol) {
    // Создаем div для графиков
    createDivsForPeriod(period);
    const allData = data
    data = data[period]['kline']
    const
        volume = [],
        dataLength = data.length;

    for (let i = 0; i < dataLength; i += 1) {
        volume.push([
            data[i][0], // the date
            data[i][5] // the volume
        ]);
    }

    const kData = allData[period]['stoch_rsi'].map(item => [item[0], item[1]]);
    const dData = allData[period]['stoch_rsi'].map(item => [item[0], item[2]]);
    const rsi6Data = allData[period]['rsi'].map(item => [item[0], item[1]]);
    const rsi12Data = allData[period]['rsi'].map(item => [item[0], item[2]]);
    const rsi24Data = allData[period]['rsi'].map(item => [item[0], item[3]]);

    const highPC = allData[period]['price_channel'].map(item => [item[0], item[1]]);
    const middlePC = allData[period]['price_channel'].map(item => [item[0], item[2]]);
    const lowPC = allData[period]['price_channel'].map(item => [item[0], item[3]]);

    const inputOrdersData = allData['orders']['orders'];
    let ordersData = [];
    if (inputOrdersData !== null) {
        ordersData = inputOrdersData.map(item => {
            const [operation, price, timestamp] = item;
            return {
                x: timestamp,
                title: operation === "sell" ? "Sell" : "Buy", // предполагая, что у нас может быть только "sell" или "buy"
                text: price,
                fillColor: operation === "sell" ? "red" : "green",
                color: 'white'
            };
        });
    }

    ordersData = ordersData.filter(item => {
        return item.x > kData[0][0];
    });

    var d = new Date();
    let chart = Highcharts.stockChart(`kline${period}`, {
        chart: {
            height: 890
        },
        title: {
            text: period
        },
        subtitle: {
            text: symbol
        },
        legend: {
            enabled: true
        },
        yAxis: [{
            height: '55%'
        }, {
            top: '55%',
            height: '35%'
        }, {
            top: '90%',
            height: '10%'
        }],
        plotOptions: {
            series: {
                showInLegend: true,
                accessibility: {
                    exposeAsGroupOnly: true
                }
            },
            candlestick: {
                color: 'red',
                upColor: 'green'
            }
        },
        series: [{
            type: 'candlestick',
            id: 'candles',
            name: 'BTCUSDT',
            data: data
        }, {
            type: 'column',
            id: 'volume',
            name: 'Volume',
            data: volume,
            yAxis: 2
        }, {
            type: 'line',
            id: 'overlay',
            linkedTo: 'candles',
            name: "High PC",
            yAxis: 0,
            data: highPC,
        }, {
            type: 'line',
            id: 'overlay',
            linkedTo: 'candles',
            name: "Middle PC",
            yAxis: 0,
            data: middlePC,
        }, {
            type: 'line',
            id: 'overlay',
            linkedTo: 'candles',
            name: "Low PC",
            yAxis: 0,
            data: lowPC,
        }, {
            type: 'line',
            id: 'k',
            name: 'K',
            data: kData,
            yAxis: 1
        }, {
            type: 'line',
            id: 'd',
            name: 'D',
            data: dData,
            yAxis: 1
        }, {
            type: 'line',
            id: 'rsi6',
            name: 'RSI6',
            data: rsi6Data,
            yAxis: 1
        }, {
            type: 'line',
            id: 'rsi12',
            name: 'RSI12',
            data: rsi12Data,
            yAxis: 1
        }, {
            type: 'line',
            id: 'rsi24',
            name: 'RSI24',
            data: rsi24Data,
            yAxis: 1
        }, {
            type: 'flags',
            allowOverlapX: true,
            accessibility: {
                exposeAsGroupOnly: true,
                description: 'Flagged events.'
            },
            name: 'Orders',
            data: ordersData,
            onSeries: 'candles',
            shape: 'circlepin',
            width: 16
        }]
    });

    charts.push(chart);
    if (document.getElementById('reload').checked) {
        chart.xAxis[0].setExtremes(
            Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours()-6, d.getMinutes() - 34 * periodExtremesMinutes[period]),
            Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours()-6, d.getMinutes()));
    }
}

function createDivsForPeriod(period) {
    const mainWrapper = document.querySelector('.main-wrapper');

    // Создаем кнопку таба для нового периода
    const tab = document.querySelector('.tab');
    const tabButton = document.createElement('button');
    tabButton.className = 'tablinks';
    tabButton.onclick = function(event) {
        openPeriod(event, period);
    };
    tabButton.innerText = period;
    tab.appendChild(tabButton);

    // Создаем контент таба для нового периода
    const tabContent = document.createElement('div');
    tabContent.id = period;
    tabContent.className = 'tabcontent col-24';

    // Создаем div для Kline согласно периоду
    const klineDiv = document.createElement('div');
    klineDiv.id = `kline${period}`;
    tabContent.appendChild(klineDiv);

    // Создаем div для StochRSI согласно периоду
    const stochrsiDiv = document.createElement('div');
    stochrsiDiv.id = `stochrsi${period}`;
    tabContent.appendChild(stochrsiDiv);

    mainWrapper.appendChild(tabContent);

    // Если это первый созданный таб, делаем его активным
    if (document.querySelectorAll('.tablinks').length === 1 && !document.getElementById('reload').checked) {
        tabButton.click();  // Эмулируем клик по кнопке, чтобы вызвать функцию openCity
    }
}
