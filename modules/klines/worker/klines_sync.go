package worker

import (
	"log"
	"time"

	"gitlab.com/Alexandrhub/tradestrategy/modules/klines/repository"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"gitlab.com/beearn/entity"
	"gitlab.com/ptflp/gopubsub/queue"
)

const NewKlinesData = "new_kline_data"

// KlinesSync is a worker for klines.
// TODO: добавить logrus
type KlinesSync struct {
	symbols          []string
	dataWarehouse    *client.WarehouseClient
	dashboardStorage repository.Cacher //
	pubsub           queue.MessageQueuer
}

type KlinesSyncConfig struct {
	Addr    string
	Timeout time.Duration
	Storage repository.Cacher
	PubSub  queue.MessageQueuer
}

func NewKlinesSync(conf KlinesSyncConfig) *KlinesSync {
	dataWarehouse := client.NewWarehouseClient(conf.Addr, conf.Timeout)
	dashboardStorage := repository.NewDashboardStorage()

	return &KlinesSync{
		dataWarehouse:    dataWarehouse,
		dashboardStorage: dashboardStorage,
		pubsub:           conf.PubSub,
	}
}

func (k *KlinesSync) Sync() {
	// TODO: оптимизировать, чтобы не ждать одну минуту в первой итерации
	ticker := time.NewTicker(1 * time.Minute)
	for {
		<-ticker.C
		if len(k.symbols) < 1 {
			k.symbols = k.dataWarehouse.GetSymbols()
		}
		for _, symbol := range k.symbols {
			data := k.dataWarehouse.GetDashboard(symbol)
			if data == nil {
				// TODO: заменить на logrus
				log.Printf("failed to get dashboard for symbol: %s", symbol)

				continue
			}
			if !k.isNew(data) {
				continue
			}
			if k.pubsub != nil {
				err := k.pubsub.Publish(NewKlinesData, []byte(data.Symbol))
				if err != nil {
					// TODO: заменить на logrus
					log.Printf("failed to publish: %s", err)
				}
			}
			k.dashboardStorage.Save(data)
		}
	}
}

// TODO: перенести метод в сервисный слой klines
func (k *KlinesSync) isNew(dashboardPeriods *entity.DashboardPeriods) bool {
	// TODO: логика проверки на новизну данных в storage
	return true
}
