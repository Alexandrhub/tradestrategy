package repository

import (
	"sync"

	"gitlab.com/beearn/entity"
)

/**
type Dashboard struct {
	Symbol string
	Period string
	Market int
	Klines []*Kline
}

type StochRSI struct {
	K []float64
	D []float64
}

type DashboardPeriods struct {
	Symbol  string
	Periods map[string]*Dashboard
}
*/

type Cacher interface {
	Save(dashboard *entity.DashboardPeriods)
	Get(symbol string) (*entity.DashboardPeriods, bool)
	Delete(symbol string)
}

type DashboardStorage struct {
	data map[string]*entity.DashboardPeriods
	lock sync.RWMutex
}

func NewDashboardStorage() *DashboardStorage {
	return &DashboardStorage{
		data: make(map[string]*entity.DashboardPeriods),
	}
}

// Save saves dashboard periods
func (s *DashboardStorage) Save(dashboard *entity.DashboardPeriods) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.data[dashboard.Symbol] = dashboard
}

// Get gets dashboard periods and bool for success
func (s *DashboardStorage) Get(symbol string) (*entity.DashboardPeriods, bool) {
	s.lock.RLock()

	val, ok := s.data[symbol]
	if !ok {
		s.lock.RUnlock()

		return nil, false
	}
	s.lock.RUnlock()

	return val, true
}

// Delete deletes dashboard period
func (s *DashboardStorage) Delete(symbol string) {
	s.lock.Lock()
	defer s.lock.Unlock()

	delete(s.data, symbol)
}
