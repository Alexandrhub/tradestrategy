# Indicator

Сервис для работы с индикаторами

Рекомендуется использовать пакеты из модуля `entity` для работы с данными
```go
package indicator

import (
	"github.com/cinar/indicator"
	"gitlab.com/beearn/entity"
	"strconv"
)
```

## Список используемых индикаторов
- Stochastic RSI (K, D) период RSI 14, сглаживание 3 сглаживание SMA
- RSI периоды 6, 12, 24
- Price Channel (Bollinger Bands) по максимальной цене закрытия

## EDA (event driven architecture)
При появлении новых данных, отправляет событие в брокер сообщений.

Данные подхватываются сервисом `order` и используются для проставления ордеров.
Новые данные сервис order получает посредством grpc, а не из брокера сообщений.
